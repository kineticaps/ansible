#!/usr/bin/python

import os
from ansible.module_utils.basic import AnsibleModule
import subprocess
import xml.etree.ElementTree as element_tree


ANSIBLE_METADATA = {'metadata_version': '1.0',
                     'status': ['stableinterface'],
                     'supported_by': 'kinetica'}


DOCUMENTATION = '''
---
module: nvidia
short_description: Gathers information about the GPU hardware and can modify some settings
description:
   - Queries hardware for GPU information
   - Sets hardware parameters
version_added: "0.1"
options:
  persistence_mode:
    description:
      - Sets the persistence mode on the GPU. Please not that this setting does NOT persist across reboots.
    required: false
    default: null
    choices: [ "enabled", "disabled", "yes", "no" ]

author: "Michael Cramer <cramer@kinetica.com>"
'''

EXAMPLES = '''
# Query GPU info and set persistence mode to enabled
- nvidia:
    persistence_mode: enabled
'''


def extract_field(node, facts, fields, prefix=None):
    for (e,t) in fields:
        key = "{}_{}".format(prefix, e) if prefix else e
        facts[key] = t(node.findall("./{}".format(e))[0].text)


def main():
    module = AnsibleModule(
        argument_spec=dict(
            persistence_mode=dict(required=False, choices=['enabled', 'disabled', '0', '1']),
            # enabled   = dict(required=True, type='bool'),
            # something = dict(aliases=['whatever'])
        )
    )
    facts = {}
    try:
        nvidia_smi = subprocess.Popen('nvidia-smi --query --xml-format', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, error = nvidia_smi.communicate()
        if nvidia_smi.returncode == 0:
            facts["nvidia_smi_installed"] = True
            root = element_tree.fromstring(output)
            extract_field(root, facts, [ ('driver_version', str), ('attached_gpus', int)], "nvidia")
            gpu_num = 0
            gpus = []
            while True:
                gpu = root.findall("./gpu[minor_number='{}']".format(gpu_num))
                if len(gpu) > 0:
                    gpu_facts = {}
                    extract_field(gpu[0], gpu_facts, [
                        ('product_name', str),
                        ('product_brand', str),
                        ('display_mode', bool),
                        ('display_active', bool),
                        ('persistence_mode', bool),
                        ('accounting_mode', bool),
                        ('accounting_mode_buffer_size', int),
                        ('minor_number', int)
                    ])
                    gpus.append(gpu_facts)
                else:
                    break
                gpu_num += 1
            if len(gpus) > 0:
                facts["nvidia_gpus"] = gpus
        elif nvidia_smi.returncode == 127:
            # Don't fail if nvidia-smi is not installed, just exit gracefully...
            facts["nvidia_smi_installed"] = False
        else:
            module.fail_json(msg="Unrecognized error code from nvidia-smi.", meta={ "error_code": nvidia_smi.returncode, "msg": error })
        module.exit_json(msg='All good', ansible_facts=facts)

    except ValueError as e:
        if e.errno == os.errno.ENOENT:
            smi_installed = False
        else:
            module.fail_json(msg="Error executing nvidia-smi.", meta=e.errno)


if __name__ == '__main__':
    main()

