#!/bin/bash

cat inventory | sed 's/.* ansible_host=\(.*\) .*/\1/g' | xargs -n 1 -I {} ssh -i ~/.ssh/kineticaps ubuntu@{} 'sudo apt install -y python'
